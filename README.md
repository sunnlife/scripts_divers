# Numéroter automatiquement un fichier FASTA

Le but de ce tuto est de changer automatiquement le nom des séquences d'un fichier FASTA en une suite de 1 à n séquences.

> ">OTU0001-KMD001 devient >1"
>
> ">OTU0201-KMD201 devient >201"
>
> et ainsi de suite

si le fichier FASTA à changer s'appelle **ancien.fasta** tapez sur un terminal le script PERL suivant

```shell
perl -ane 'if(/\>/){$a++;print ">$a\n"}else{print;}' ancien.fasta > nouveau.fasta 
```

Le fichier **nouveau.fasta** est prêt à l'emploi

